﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppCambiarColor
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void SL_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            var rojo = this.slRojo.Value;
            var verde = this.slVerde.Value;
            var azul = this.slAzul.Value;
            var opacidad = this.slOpacidad.Value;
            Color nuevoColor = new Color(rojo, verde, azul, opacidad);
            this.boxColor.BackgroundColor = nuevoColor;
            this.lbDisplay.Text = ColorHexa(nuevoColor);
        }
        private string ColorHexa(Color nuevoColor)
        {
            int rojo = (int)(nuevoColor.R * 255);
            int verde = (int)(nuevoColor.G * 255);
            int azul = (int)(nuevoColor.B * 255);
            int opacidad = (int)(nuevoColor.A * 255);
            return $"#{rojo:x2}{verde:x2}{azul:x2}{opacidad:x2}";

        }
    }
}
